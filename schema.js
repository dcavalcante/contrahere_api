const mongoose = require('mongoose');
const fs = require('fs');

const {ObjectId} = mongoose.Types;
const {
	GraphQLID,
	GraphQLString,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema,
  GraphQLInt,
  GraphQLScalarType,
  GraphQL,
} = require("graphql");

const {
  GraphQLDateTime
} = require('graphql-iso-date');

const { GraphQLUpload } = require('graphql-upload');

const {
  ContractModel,
  PartyModel,
} = require('./models');

mongoose.connect('mongodb://localhost/contrahere');

const ContractType = new GraphQLObjectType({
	name: "Contract",
	fields: {
		id: {type: GraphQLID},
		title: {type: GraphQLString},
		startDate: {type: GraphQLDateTime},
		endDate: {type: GraphQLDateTime},
		pdf: {type: GraphQLUpload},
		filename: {type: GraphQLString},
		parties: {type: GraphQLList(GraphQLID)},
	}
});

const PartyType = new GraphQLObjectType({
	name: "Party",
	fields: {
		id: {type: GraphQLID},
		firstname: {type: GraphQLString},
		lastname: {type: GraphQLString},
		email: {type: GraphQLString},
		cpf: {type: GraphQLString},
		phone: {type: GraphQLString},
	}
})

// const Party = mongoose.model('')
const schema = new GraphQLSchema({
	query: new GraphQLObjectType({
		name: "Query",
		fields: {
			contracts: {
				type: GraphQLList(ContractType),
				resolve: (root, args, context, info) => {
					return ContractModel.find().exec();
				}
			},
			contract: {
				type: ContractType,
				args: {
					id: { type: GraphQLNonNull(GraphQLID)},
				},
				resolve: (root, args, context, info) => {
					return ContractModel.findById(args.id).exec();
				}
			},
			parties: {
				type: GraphQLList(PartyType),
				resolve: (root, args, context, info) => {
					return PartyModel.find().exec();
				}
			},
			party: {
				type: PartyType,
				args: {
					id: { type: GraphQLNonNull(GraphQLID) },
				},
				resolve: (root, args, context, info) => {
					return PartyModel.findById(args.id).exec();
				}
			},
		},
	}),
	mutation: new GraphQLObjectType({
		name: "Mutation",
		fields: {
			contractAdd: {
				type: ContractType,
				args: {
					title: { type: GraphQLNonNull(GraphQLString)},
					startDate: { type: GraphQLNonNull(GraphQLDateTime)},
					endDate: { type: GraphQLNonNull(GraphQLDateTime)},
					pdf: { type: GraphQLNonNull(GraphQLUpload) },
					parties: {type: GraphQLList(GraphQLID)},
				},
				resolve: async (root, args, context, info) => {
					let contract = new ContractModel(args);
					const {filename, mimetype, createReadStream} = await args.pdf;
					// let result = contract._id + filename;
					// contract.filename = result;
					
					const stream = createReadStream();
					// let write = fs.createWriteStream(`./uploads/${result}`);
					let write = fs.createWriteStream(`./uploads/${contract._id}.pdf`);
					stream.pipe(write);
					
					// console.log('args', args);
					// console.log(filename);
					// console.log(result);
					// const result = Math.floor(Date.now() / 1000) + filename;
					// console.log(stream);
					// const {pdf} = await args;
					// console.log(pdf)

					return contract.save();
				}
      },
      contractsRemove: {
        type: ContractType,
        args: {
          ids: {type: GraphQLNonNull(GraphQLList(GraphQLString))}
        },
        resolve: async (root, args, context, info) => {
          let ids = args.ids.map(id => ObjectId(id))
          let removed = await ContractModel.deleteMany({_id: {$in:args.ids}});
          return removed;
        }
      },
      contractUpdate: {
        type: ContractType,
        args: {
          id: {type: GraphQLNonNull(GraphQLID)},
          title: { type: GraphQLString},
					startDate: { type: GraphQLDateTime},
					endDate: { type: GraphQLDateTime},
					parties: {type: GraphQLList(GraphQLID)},
					pdf: { type: GraphQLUpload },					
        },
        resolve: async (root, args, context, info) => {
          let contract = await ContractModel.findByIdAndUpdate(args.id, args, {new:true});
          // let contract = await ContractModel.findById(args.id);
					

					if (args.pdf) {
						// fs.unlinkSync(`./uploads/${contract.filename}`);
						const {filename, mimetype, createReadStream} = await args.pdf;
						// let result = contract._id + filename;
						// contract.filename = result;
						
						const stream = createReadStream();
						let write = fs.createWriteStream(`./uploads/${contract._id}.pdf`);
						stream.pipe(write);
					}
					
					
					// console.log(args);

					// return contract.save();
        }
      },
			partyAdd: {
				type: PartyType,
				args: {
					firstname: { type: GraphQLNonNull(GraphQLString)},
					lastname: { type: GraphQLNonNull(GraphQLString)},
					email: { type: GraphQLNonNull(GraphQLString)},
					cpf: { type: GraphQLNonNull(GraphQLString)},
					phone: { type: GraphQLNonNull(GraphQLString)},
				},
				resolve: (root, args, context, info) => {
					let party = new PartyModel(args);
					return party.save();
				}
      },
      partiesRemove: {
        type: PartyType,
        args: {
          ids: {type: GraphQLNonNull(GraphQLList(GraphQLString))}
        },
        resolve: async (root, args, context, info) => {
          let ids = args.ids.map(id => ObjectId(id))
          let removed = await PartyModel.deleteMany({_id: {$in:args.ids}});
          return removed;
        }
			},
			partyUpdate: {
        type: PartyType,
        args: {
          id: {type: GraphQLNonNull(GraphQLID)},
          firstname: { type: GraphQLString},
					lastname: { type: GraphQLString},
					email: { type: GraphQLString},
					cpf: { type: GraphQLString},
					phone: { type: GraphQLString}
        },
        resolve: async (root, args, context, info) => {
          let party = await PartyModel.findByIdAndUpdate(args.id, args, {new:true});
          return party;
        }
      },
		},
	}),
});

module.exports = schema;