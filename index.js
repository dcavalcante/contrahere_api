const express = require('express');
const graphqlHTTP = require('express-graphql');
const { graphqlUploadExpress } = require('graphql-upload');
const schema = require('./schema');
const cors = require('cors');

const app = express();

app.use(cors());

app.use('/uploads', express.static('./uploads'))
// app.use(express.static('/uploads', express.static(__dirname + '/uploads')));
// app.use('/uploads',express.static(path.join(__dirname, 'uploads')));
app.use('/graphql', 
	graphqlUploadExpress(),
	graphqlHTTP({
		schema,
		rootValue: global,
		graphiql: true,
	})
);

app.listen(4000);
console.log('Listening on port 4000');