const mongoose = require('mongoose');

const ContractModel = mongoose.model('contract', {
	title: String,
	startDate: Date,
	endDate: Date,
	parties: [String],
	filename: String,
	// file: Buffer,
});

const PartyModel = mongoose.model('party', {
	firstname: String,
	lastname: String,
	email: String,
	cpf: {type: String, min: 12, max: 12},
	phone: {type: String, min: 4},
	// cpf: {type: Number, min: 12, max: 12},
	// phone: {type: Number, min: 4},
});

module.exports = {
  ContractModel,
  PartyModel,
}