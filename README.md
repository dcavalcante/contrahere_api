This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install

To install project just run:

### `npm install`

## Running

To run the backend type:
### `node index.js`

Runs the app in the development mode at port 4000.

## Dependencies

**Warning: You must have mongodb installed and running on the default port 27018. Otherwise you need to change setup on index.js!**